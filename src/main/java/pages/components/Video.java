package pages.components;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import pages.Page;
import pages.PageManager;

import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertTrue;

public class Video extends Page {

    private By IMAGE = xpath("//div[contains(@class,'serp-item_layout_horizontal')]//img");
    private By SELECTED_IMAGE = xpath("//div[contains(@class,'thumb-image_hovered')]//img");

    public Video(PageManager pages) {
        super(pages);
    }

    @Step("Трейлер отображается")
    public void assertTrailer() throws InterruptedException {
        hover(IMAGE, 3);
        //System.out.println(getAttribute(SELECTED_IMAGE, "srcset"));
        assertTrue(getAttribute(SELECTED_IMAGE, "srcset").contains("https://avatars.mds.yandex.net/get-video_frame/"));
    }
}
