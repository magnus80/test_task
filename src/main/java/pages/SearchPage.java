package pages;

import io.qameta.allure.Step;

import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.xpath;
import static utils.PropertyLoader.loadProperty;

public class SearchPage extends Page {

    private static final String SEARCH_FIELD = "input__control";
    private static final String TORNADO = "ураган";
    private static final String SEARCH_BUTTON = "//button[@type='submit']";

    SearchPage(PageManager pages) {
        super(pages);
    }

    @Step("Открываем страницу Яндекс.Видео")
    public SearchPage openSearchPage() {
        open(loadProperty("web.baseUrl"));
        return this;
    }

    @Step("Поиск видео по слову 'ураган'")
    public void search() {
        type(className(SEARCH_FIELD), TORNADO);
        clickLMB(xpath(SEARCH_BUTTON));
    }
}
