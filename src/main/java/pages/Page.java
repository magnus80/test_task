package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static java.lang.Thread.sleep;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.testng.Assert.assertEquals;

public class Page {
    WebDriver wd;
    private final PageManager pages;

    public Page(PageManager pages) {
        this.pages = pages;
        wd = pages.getWebDriver();
    }

    void open(String url) {
        wd.get(url);
    }

    void type(By locator, String text) {
        clickLMB(locator);
        if (text != null) {
            String existingText = wd.findElement(locator).getAttribute("value");
            if (!text.equals(existingText)) {
                wd.findElement(locator).clear();
                wd.findElement(locator).sendKeys(text);
            }
        }
    }

    Page clickLMB(By locator) {
        wd.findElement(locator).click();
        return this;
    }

    Page waitForElementVisibility(By element, int seconds) {
        WebDriverWait wait = new WebDriverWait(wd, seconds);
        wait.until(presenceOfElementLocated(element));
        return this;
    }

    Page assertQuantity(By element, int quantity) {
        assertEquals(wd.findElements(element).size(), quantity);
        return this;
    }

    protected Page hover(By element, int i) throws InterruptedException {
        Actions actions = new Actions(wd);
        actions.moveToElement(wd.findElements(element).get(i)).build().perform();
        sleepFor(2000);
        return this;
    }

    protected String getAttribute(By element, String atrName) {
        return wd.findElement(element).getAttribute(atrName);
    }

    static void sleepFor(long timeout) throws InterruptedException {
        sleep(timeout);
    }


}
