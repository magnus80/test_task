package pages;

import org.openqa.selenium.WebDriver;
import pages.components.Video;

public final class PageManager {
    private WebDriver driver;
    private final SearchPage searchPage;
    private final SearchResultsPage searchResultsPage;
    private final Video video;

    public PageManager(WebDriver driver) {
        this.driver = driver;
        searchPage = new SearchPage(this);
        searchResultsPage = new SearchResultsPage(this);
        video = new Video(this);
    }

    public SearchPage searchPage() {
        return searchPage;
    }

    public SearchResultsPage searchResultsPage() {
        return searchResultsPage;
    }

    public Video video() {
        return video;
    }

    WebDriver getWebDriver() {
        return driver;
    }

}