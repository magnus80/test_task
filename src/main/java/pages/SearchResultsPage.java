package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static org.openqa.selenium.By.xpath;

public class SearchResultsPage extends Page {

    private static final By RESULTS = xpath("//div[contains(@class,'serp-item_layout_horizontal')]");

    SearchResultsPage(PageManager pages) {
        super(pages);
    }

    @Step("Проверяем, что нашлось \"{0}\" видео роликов")
    public void assertQuantity(int quantity) {
        waitForElementVisibility(RESULTS, 5);
        assertQuantity(RESULTS, quantity);
    }

}
