package utils;

import io.qameta.allure.Attachment;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.openqa.selenium.OutputType.BYTES;

public class AllureReportListener implements ITestListener {
    @Override
    public void onTestStart(ITestResult iTestResult) {
        // Don't use
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        makeScreenshot();
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        makeScreenshot();
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        makeScreenshot();
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        // Don't use
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        // Don't use
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        // Don't use
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    private byte[] makeScreenshot() {
        return ((TakesScreenshot) chromedriver()).getScreenshotAs(BYTES);
    }
}
