package utils;

import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {

    private PropertyLoader() {
    }

    public static String loadProperty(String name) {

        Properties properties = new Properties();
        try {
            String prop_file = "/application.properties";
            properties.load(PropertyLoader.class.getResourceAsStream(prop_file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String value = "";

        if (name != null) {
            value = properties.getProperty(name);
        }
        return value;
    }

}
