package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import pages.PageManager;
import pages.SearchPage;
import pages.SearchResultsPage;
import pages.components.Video;
import utils.AllureReportListener;

import static java.util.concurrent.TimeUnit.SECONDS;

@Listeners(AllureReportListener.class)
public class TestBase {
    private PageManager pageManager;
    private WebDriver wd;

    @BeforeSuite
    public void init() {
        WebDriverManager.chromedriver().setup();
        wd = new ChromeDriver();
        wd.manage().timeouts().implicitlyWait(5, SECONDS);
    }

    @AfterSuite
    public void tearDown() {
        wd.quit();
    }

    @BeforeClass
    public void createPageManager() {
        createNewPageManager();
    }

    private void createNewPageManager() {
        pageManager = new PageManager(wd);
    }

    SearchPage searchPage() {
        return pageManager.searchPage();
    }

    SearchResultsPage searchResultsPage() {
        return pageManager.searchResultsPage();
    }

    Video video() {
        return pageManager.video();
    }
}
