package tests;

import com.automation.remarks.testng.VideoListener;
import com.automation.remarks.video.annotations.Video;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(VideoListener.class)
public class TrailerTest extends TestBase {

    @Test
    @Video
    public void testExistingTrailer() throws InterruptedException {
        searchPage().openSearchPage().search();
        searchResultsPage().assertQuantity(20);
        video().assertTrailer();
    }
}
